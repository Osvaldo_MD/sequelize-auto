# sequelize-auto

Actualización de [Sequelize-Auto](https://github.com/sequelize/sequelize-auto#readme), para utilizarlo hay que instalar previamente ```sequelize-auto``` 
```
npm i -g sequelize-auto
```
descomprimir el archivo ```.rar``` en la siguiente ruta
```
C:\Users\"usuario"\AppData\Roaming\npm\node_modules
```

Si se presenta errores al ejecutar el comando de creación, es posible que se necesite instalar de forma global o local 
los siguientes modulos, de forma global:
```
npm i -g lodash
npm i -g sequelize
```
y para local hay que ir al directorio donde se descomprimió el archivo ```.rar```, en la barra de navegación
escribir ```cmd``` y dar click en la tecla ```Enter ↩```, posteriormente, en la terminal ejecutar el comando
```
npm i lodash@4.17.11 & npm i sequelize@5.8.0
```


Osvaldo Miranda Durán - 29/04/2019.